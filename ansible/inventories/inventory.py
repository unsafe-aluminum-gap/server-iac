#!/usr/bin/env python3

# This script creates an inventory file to be used by Ansible.
#
# usage: inventory.py [-h] [-i INVENTORY] [-f VAULT_PASSWORD_FILE]
#
# options:
#   -h, --help            show this help message and exit
#   -i INVENTORY, --inventory INVENTORY
#                         path to the dynamic inventory file
#   -f VAULT_PASSWORD_FILE, --vault-password-file VAULT_PASSWORD_FILE
#                         path to file containing the password to the Ansible Vault
#
# Requirements:
#   PyYAML

from argparse import ArgumentParser
from pathlib import Path
import subprocess
import yaml
import sys

def main() -> int:
    args = parse_arguments()

    # get_hosts_data(args.inventory)

    result = read_ansible_vault(args.vault_file, args.vault_password_file)

    print(result)

    return 0


def read_ansible_vault(vault: Path, password_file: Path) -> dict:
    '''
    Calls ansible-vault to read the vault file provided, returning it as a
    dictionary.
    '''
    vault_content = subprocess.run(
        [
            'ansible-vault',
            'view',
            '--vault-password-file',
            password_file,
            vault
        ],
        capture_output=True,
        text=True,
        check=True
    )

    vault_dict = {}

    for line in vault_content.stdout.strip().split('\n'):
        key, value = line.split(': ')
        vault_dict[key] = value

    return vault_dict


def get_hosts_data(inventory: Path) -> str:
    '''
    Calls ansible-inventory using the provided inventory file.
    '''
    result = subprocess.run(
        ['ansible-inventory', '--list', '-i', Path.resolve(inventory)],
        capture_output=True,
        text=True,
        check=True
    )

    return result.stdout


def parse_arguments():
    parser = ArgumentParser()

    parser.add_argument(
        '-i',
        '--inventory',
        help="path to the dynamic inventory file.",
        type=Path,
    )
    parser.add_argument(
        '-v',
        '--vault-file',
        help="path to Ansible Vault file containing encrypted secrets.",
        type=Path,
        default=f'{Path(__file__).parent}/group_vars/all/vault.yml',
    )
    parser.add_argument(
        '-f',
        '--vault-password-file',
        help="path to file containing the password to the Ansible Vault.",
        type=Path,
        default=f'{Path.home()}/.ansible/ansible_vault_ssh_pass',
    )

    return parser.parse_args()


if __name__ == '__main__':
    sys.exit(main())
