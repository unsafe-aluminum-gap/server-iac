user  nginx;
worker_processes  auto;

error_log     /dev/stderr;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    access_log    /dev/stdout;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    server {
      listen 80;
      server_name devontheroof.top www.devontheroof.top;

      root /var/www/html;
      index index.php;

      # Instructs nginx to use local resolver
      resolver 127.0.0.11 ipv6=off valid=1s;

      # Returns .data object generated by emscripten
      location ~ \.data$ {
        root /var/www/html/wp-content/themes/devontheroof/assets;
      }

      location / {
        try_files $uri $uri/ /index.php$is_args$args;
        #include /etc/nginx/config/wp_super_cache.conf;
      }

      location ~ \.php {
        # regex to split $uri to $fastcgi_script_name and $fastcgi_path
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;

        # Check that the PHP script exists before passing it
        try_files $fastcgi_script_name =404;

        # Bypass the fact that try_files resets $fastcgi_path_info
        # see: http://trac.nginx.org/nginx/ticket/321
        set $path_info $fastcgi_path_info;
        fastcgi_param PATH_INFO $path_info;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include /etc/nginx/fastcgi_params;

        # include /etc/nginx/global/fastcgi.conf;
        fastcgi_index index.php;
        fastcgi_pass wordpress:9000;
        fastcgi_buffer_size 32k;
        fastcgi_buffers 16 16k;
        fastcgi_busy_buffers_size 64k;
        fastcgi_temp_file_write_size 64k;
      }
    }
}
