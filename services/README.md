All my services expressed as code and text-based configuration files. These is a tree representation of the directories:

```
services
├── devontheroof
│   ├── docker-compose.yml
│   ├── files
│   └── README.md
├── jenkins
│   ├── docker-compose.yml
│   └── README.md
├── metrics
│   ├── docker-compose.yml
│   ├── files
│   └── README.md
├── README.md
└── reverse-proxy
    ├── docker-compose.yml
    ├── files
    └── README.md
```