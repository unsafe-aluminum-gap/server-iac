### Dev On The Roof

My personal site for notes and showcasing my portfolio projects.

This service expects to be deployed inside a Docker Swarm. It does not expose any ports to the public internet. Instead, it tries to connect to an external overlay network named *reverse-proxy*.

See the code:
https://github.com/herokunt/developer-sojourn

```
devontheroof
├── docker-compose.yml
├── files
│   ├── nginx
│   │   ├── config
│   │   │   ├── basics.conf
│   │   │   ├── buffers.conf
│   │   │   ├── cache.conf
│   │   │   ├── compression.conf
│   │   │   ├── fastcgi_optimize.conf
│   │   │   ├── fastcgi_php.conf
│   │   │   ├── headers.conf
│   │   │   ├── logging.conf
│   │   │   ├── optimizations.conf
│   │   │   ├── rate_limit.conf
│   │   │   ├── timeout.conf
│   │   │   ├── wp_security.conf
│   │   │   └── wp_super_cache.conf
│   │   ├── default.conf
│   │   └── nginx.conf
│   ├── php
│   │   └── custom.ini
│   └── plugins
│       └── prism
│           ├── build
│           │   ├── index.asset.php
│           │   └── index.js
│           ├── index.php
│           └── src
│               └── index.js
└── README.md
```
