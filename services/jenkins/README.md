The docker image used for this service is based on jenkins/jenkins:lts and includes the docker binaries to run docker containers in the host. Binding the docker socket file is therefore required when running the container.

Port 8080 is not exposed on the host as it runs on the same network as the reverse proxy. Port 50000 is also not exposed as it's used to when using Jenkins with workers, not used at the moment.